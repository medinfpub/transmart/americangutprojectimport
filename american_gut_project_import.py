# -*- coding: utf-8 -*-

"""american_gut_project_import.py

Author: Alexander Birkenkamp
E-mail: alexander.birkenkamp@med.uni-goettingen.de
Version: 1.0
Last update: 27th June 2019

This script collects and integrates data about participants of the
American Gut Project [1]. Proband-related data and data about their
samples are located at BioSamples [2], metagenomic data at MGnify [3].
Several files for a tranSMART batch input are created through this
script, namely annotations.tsv, subjects_data.tsv,
excluded_subjects_data.tsv, metagenomic_data.tsv, and mappings.tsv.

The program was successfully used with Python 3.5.2 on a server with
Ubuntu 16.04.3 as operating system. It is assumed that the script is run
in the directory containing the tab separated file with the analysis
results of the American Gut Project from MGnify (file:
ERP012803_taxonomy_abundances_v2.0.tsv). The program can be tested with
data related to a variable range of sequencing run identifiers from this
file by adding the options "--start" (or "-s") or "--end" (or "-e") with
integers on the command line.

References:
    [1] URL: http://americangut.org/ (last access on 27th March 2019)
    [2] URL: https://www.ebi.ac.uk/biosamples/ (last access on 27th
        March 2019)
    [3] URL: https://www.ebi.ac.uk/metagenomics/ (last access on 27th
        March 2019)
"""

import argparse
import datetime
import functools
import math
import multiprocessing
import os
import requests
import sys
import time

# The file with the analysis results of the American Gut Project, which
# has been downloaded from MGnify, and its encoding.
TAX_ABU_FILE = 'ERP012803_taxonomy_abundances_v2.0.tsv'
TAX_ABU_FILE_ENC = 'utf-8'

# The directory with the file ERP012803_taxonomy_abundances_v2.0.tsv.
DIR = './'

# The URLs to the RESTful APIs of MGnify and BioSamples.
MGNIFY_URL = 'https://www.ebi.ac.uk/metagenomics/api/latest/'
BIOSAMPLES_URL = 'https://www.ebi.ac.uk/biosamples/samples/'

# The names of the output files.
ANNS_FILE = 'annotations.tsv'
SUBJS_FILE = 'subjects_data.tsv'
EXCL_SUBJS_FILE = 'excluded_subjects_data.tsv'
METAGEN_FILE = 'metagenomic_data.tsv'
MAPS_FILE = 'mappings.tsv'

# The attributes referring to the participants of the American Gut
# Project (and their samples). They are gathered both from a
# questionnaire [4] and a data dictionary [5] in files, which were
# originally published with the article "American Gut: an Open Platform
# for Citizen Science Microbiome Research" by McDonald et al. [2018]
# (DOI: 10.1128/mSystems.00031-18).
#
# References:
#    [4] File: mcdonald_et_al_2018_supplement_with_questionnaire.docx
#    [5] File: mcdonald_et_al_2018_supplement_with_data_dictionary.xlsx
ATTRS = [
    # Personal and contact information.
    'sex',
    'age_years',
    'bmi',
    'height_cm',
    'weight_kg',
    'country_of_birth',
    'census_region',
    'economic_region',
    'state',
    'country_residence',
    # General diet information.
    'diet_type',
    'multivitamin',
    'probiotic_frequency',
    'vitamin_b_supplement_frequency',
    'vitamin_d_supplement_frequency',
    'other_supplement_frequency',
    'specialized_diet_exclude_dairy',
    'specialized_diet_exclude_nightshades',
    'specialized_diet_exclude_refined_sugars',
    'specialized_diet_fodmap',
    'specialized_diet_halaal',
    'specialized_diet_i_do_not_eat_a_specialized_diet',
    'specialized_diet_kosher',
    'specialized_diet_modified_paleo_diet',
    'specialized_diet_other_restrictions_not_described_here',
    'specialized_diet_paleodiet_or_primal_diet',
    'specialized_diet_raw_food_diet',
    'specialized_diet_unspecified',
    'specialized_diet_westenprice_or_other_lowgrain_low_processed_fo',
    'consume_animal_products_abx',
    'drinking_water_source',
    # General information.
    'race',
    'last_move',
    'last_travel',
    'roommates',
    'roommates_in_study',
    'livingwith',
    'dog',
    'cat',
    'pets_other',
    'dominant_hand',
    'level_of_education',
    # General lifestyle and hygiene information.
    'exercise_frequency',
    'exercise_location',
    'nail_biter',
    'pool_frequency',
    'smoking_frequency',
    'alcohol_consumption',
    'alcohol_frequency',
    'alcohol_types_beercider',
    'alcohol_types_red_wine',
    'alcohol_types_sour_beers',
    'alcohol_types_spiritshard_alcohol',
    'alcohol_types_unspecified',
    'alcohol_types_white_wine',
    'drinks_per_session',
    'teethbrushing_frequency',
    'flossing_frequency',
    'cosmetics_frequency',
    'deodorant_use',
    'sleep_duration',
    'softener',
    # General health information.
    'bowel_movement_frequency',
    'bowel_movement_quality',
    'antibiotic_history',
    'flu_vaccine_date',
    'contraceptive',
    'pregnant',
    'weight_change',
    'tonsils_removed',
    'appendix_removed',
    'chickenpox',
    'acne_medication',
    'acne_medication_otc',
    'csection',
    'fed_as_infant',
    'add_adhd',
    'alzheimers',
    'lung_disease',
    'asd',
    'autoimmune',
    'fungal_overgrowth',
    'cdiff',
    'cardiovascular_disease',
    'mental_illness',
    'mental_illness_type_anorexia_nervosa',
    'mental_illness_type_bipolar_disorder',
    'mental_illness_type_bulimia_nervosa',
    'mental_illness_type_depression',
    'mental_illness_type_ptsd_posttraumatic_stress_disorder',
    'mental_illness_type_schizophrenia',
    'mental_illness_type_substance_abuse',
    'mental_illness_type_unspecified',
    'diabetes',
    'diabetes_type',
    'epilepsy_or_seizure_disorder',
    'ibs',
    'ibd',
    'ibd_diagnosis',
    'ibd_diagnosis_refined',
    'migraine',
    'kidney_disease',
    'liver_disease',
    'pku',
    'sibo',
    'skin_condition',
    'thyroid',
    'seasonal_allergies',
    'non_food_allergies_beestings',
    'non_food_allergies_drug_eg_penicillin',
    'non_food_allergies_pet_dander',
    'non_food_allergies_poison_ivyoak',
    'non_food_allergies_sun',
    'non_food_allergies_unspecified',
    'lactose',
    'gluten',
    'allergic_to_i_have_no_food_allergies_that_i_know_of',
    'allergic_to_other',
    'allergic_to_peanuts',
    'allergic_to_shellfish',
    'allergic_to_tree_nuts',
    'allergic_to_unspecified',
    'vivid_dreams',
    # Detailed dietary information.
    'breastmilk_formula_ensure',
    'meat_eggs_frequency',
    'homecooked_meals_frequency',
    'ready_to_eat_meals_frequency',
    'prepared_meals_frequency',
    'whole_grain_frequency',
    'fruit_frequency',
    'vegetable_frequency',
    'types_of_plants',
    'fermented_plant_frequency',
    'milk_cheese_frequency',
    'milk_substitute_frequency',
    'frozen_dessert_frequency',
    'red_meat_frequency',
    'high_fat_red_meat_frequency',
    'poultry_frequency',
    'seafood_frequency',
    'salted_snacks_frequency',
    'sugary_sweets_frequency',
    'olive_oil',
    'whole_eggs',
    'sugar_sweetened_drink_frequency',
    'artificial_sweeteners',
    'one_liter_of_water_a_day_frequency',
    # Sample type.
    'sample_type']
# Numerical attributes.
NUM_ATTRS = ['age_years', 'bmi', 'height_cm', 'weight_kg']

def read_tax_abu_file(start: int, end: int) -> tuple:
    """Extract the sequencing run identifiers and analysis results from
    the file ERP012803_taxonomy_abundances_v2.0.tsv in the given
    range."""
    try:
        rf = open(DIR + TAX_ABU_FILE, encoding=TAX_ABU_FILE_ENC)
    except FileNotFoundError:
        sys.exit('File ' + TAX_ABU_FILE + ' not found.')
    else:
        lines = rf.readlines()
        run_ids = lines[0].rstrip('\n').split('\t')[1:][start:end]
        analysis = {}
        for l in lines[1:]:
            group, counts = l.split('\t', 1)
            # Shorten the name of the group to a maximum of 13
            # characters per level in the biological taxonomy.
            short_group = ';'.join([
                level_name[:12] + '.' if len(level_name) > 13 else level_name
                for level_name in group.split(';')])
            analysis[short_group] = counts.rstrip('\n').split('\t')[start:end]
        rf.close()
        return run_ids, analysis

def part(run_ids: list) -> list:
    """Partition sequencing run identifiers into multiple sets depending
    on the number of available CPUs for processing them parallelly."""
    num_data = len(run_ids)
    chunk_size = math.ceil(num_data / os.cpu_count())
    return [run_ids[i : i+chunk_size] for i in range(0, num_data, chunk_size)]

def get_resps(urls: list) -> list:
    """Request the given URLs and get the responses in JSON format."""
    return [requests.get(u).json() for u in urls]

def get_mgnify_sample_ids(run_ids: list) -> list:
    """Collect MGnify's sample identifiers corresponding to the given
    identifiers of sequencing runs."""
    mgnify_sample_ids = []
    urls = [MGNIFY_URL + 'runs/' + r for r in run_ids]
    for resp in get_resps(urls):
        sample_id = resp['data']['relationships']['sample']['data']['id']
        mgnify_sample_ids.append(sample_id)
    return mgnify_sample_ids

def get_biosamples_sample_ids(mgnify_sample_ids: list) -> list:
    """Collect the sample identifiers from BioSamples corresponding to
    the given sample identifiers from MGnify."""
    biosamples_sample_ids = []
    urls = [MGNIFY_URL + 'samples/' + m for m in mgnify_sample_ids]
    for resp in get_resps(urls):
        sample_id = resp['data']['attributes']['biosample']
        biosamples_sample_ids.append(sample_id)
    return biosamples_sample_ids

def get_subjs_samples_data(biosamples_sample_ids: list) -> list:
    """Collect data about the subjects and their samples associated with
    the given sample identifiers from BioSamples."""
    subjs_samples_data = []
    urls = [BIOSAMPLES_URL + b for b in biosamples_sample_ids]
    for resp in get_resps(urls):
        vals = []
        chars = resp['characteristics']
        # Try to extract the host subject identifier. If this id cannot
        # be determined for a sample identifier from BioSamples, the
        # subject is excluded from the data.
        try:
            hsid = chars['host_subject_id'][0]['text']
        except KeyError:
            subjs_samples_data.append(('NO_SUBJ_ID'))
            continue
        vals.append(hsid)
        # Try to get the values for the attributes listed above. If an
        # attribute's value is not entered at all (inferred from a key
        # error), it is set to "-1" for numerical attributes (age, BMI,
        # height, and weight) and to "Not entered" for categorical
        # attributes (the remaining ones). If a numerical attribute's
        # value equals "Not applicable" or "Not provided", it is set to
        # "-1".
        for a in ATTRS:
            try:
                val = chars[a][0]['text']
            except KeyError:
                val = '-1' if a in NUM_ATTRS else 'Not entered'
            if a in NUM_ATTRS \
                    and (val == 'Not applicable' or val == 'Not provided'):
                val = '-1'
            vals.append(val)
        subjs_samples_data.append(tuple(vals))
    return subjs_samples_data

def req_workflow(run_ids: list) -> list:
    """Request data about subjects and their samples with respect to the
    given sequencing run identifiers."""
    mgnify_sample_ids = get_mgnify_sample_ids(run_ids)
    biosamples_sample_ids = get_biosamples_sample_ids(mgnify_sample_ids)
    return get_subjs_samples_data(biosamples_sample_ids)

def check_lens(req_data: list, run_ids: list) -> None:
    """Verify that the given lists share the same number of items."""
    if len(req_data) != len(run_ids):
        sys.exit('Inequal numbers of requested data sets and sequencing run '
                 'ids.')

def filt(req_data: list, run_ids: list) -> tuple:
    """Remove data sets without host subject identifier and the
    corresponding sequencing run identifiers."""
    filt_req_data = []
    filt_run_ids = []
    for re, ru in zip(req_data, run_ids):
        hsid = re[0]  # Host subject id.
        if hsid != 'NO_SUBJ_ID':
            filt_req_data.append(re)
            filt_run_ids.append(ru)
    return filt_req_data, filt_run_ids

def short_subj_ids(req_data: list) -> list:
    """Replace the host subject identifiers given in the requested data
    by shorter ones."""
    # As the tranSMART database, into which the subjects' data are
    # loaded, cannot store the host subject ids from BioSamples due to
    # their length, shorter subject ids are created, starting from value
    # 1,000. The original subject ids from BioSamples are kept and
    # uniquely mapped to the new ones.
    short_req_data = []
    subj_ids = {}
    new_subj_id = 1000
    for r in req_data:
        hsid = r[0]  # Host subject id.
        if hsid in subj_ids.keys():
            subj_id = subj_ids[hsid]
        else:
            subj_id = subj_ids.setdefault(hsid, new_subj_id)
            new_subj_id += 1
        short_req_data.append((subj_id,) + r[1:])
    return short_req_data

def count_num_subjs(req_data: list) -> tuple:
    """Determine the total number of subjects as well as the numbers of
    included and excluded subjects."""
    incl_subjs = set()
    excl_subjs = set()
    for r in req_data:
        subj_id, age, bmi, height, weight = r[0], r[2], r[3], r[4], r[5]
        # If a numerical attribute's value equals "-1", the subject is
        # supposed to be excluded.
        if age == '-1' or bmi == '-1' or height == '-1' or weight == '-1':
            excl_subjs.add(subj_id)
        else:
            incl_subjs.add(subj_id)
    num_subjs = len(incl_subjs.union(excl_subjs))
    num_incl = len(incl_subjs)
    num_incl_excl = len(incl_subjs.intersection(excl_subjs))
    num_excl = len(excl_subjs) - num_incl_excl
    return num_subjs, num_incl, num_excl, num_incl_excl

def count_no_num_data(req_data: list) -> tuple:
    """Determine the numbers of data sets with unprovided age, BMI,
    height, or weight values."""
    num_no_age = 0
    num_no_bmi = 0
    num_no_height = 0
    num_no_weight = 0
    for r in req_data:
        age, bmi, height, weight = r[2], r[3], r[4], r[5]
        if age == '-1':
            num_no_age += 1
        if bmi == '-1':
            num_no_bmi += 1
        if height == '-1':
            num_no_height += 1
        if weight == '-1':
            num_no_weight += 1
    return num_no_age, num_no_bmi, num_no_height, num_no_weight

def write_anns_file(analysis: dict) -> None:
    """Create the annotations file."""
    with open(DIR + ANNS_FILE, 'w') as wf:
        for short_group in analysis.keys():
            wf.write(
                '\t'.join(['platform', short_group, '...', '0', 'organism'])
                + '\n')

def write_subjs_files(req_data: list) -> None:
    """Create the two subjects files (for included and excluded
    subjects, respectively)."""
    seen_subj_ids = []
    with open(DIR + SUBJS_FILE, 'w') as wf_incl_subjs, \
            open(DIR + EXCL_SUBJS_FILE, 'w') as wf_excl_subjs:
        header = '\t'.join(['subject_id'] + ATTRS[:-1]) + '\n'
        wf_incl_subjs.write(header)
        wf_excl_subjs.write(header)
        for r in req_data:
            subj_id, age, bmi, height, weight = r[0], r[2], r[3], r[4], r[5]
            if subj_id not in seen_subj_ids:
                seen_subj_ids.append(subj_id)
                line = '\t'.join([str(r[i]) for i in range(len(r)-1)]) + '\n'
                # If a numerical attribute's value equals "-1", the
                # subject is supposed to be excluded.
                if age == '-1' or bmi == '-1' \
                        or height == '-1' or weight == '-1':
                    wf_excl_subjs.write(line)
                else:
                    wf_incl_subjs.write(line)

def write_metagen_file(run_ids: list, analysis: dict) -> None:
    """Create the metagenomic data file."""
    with open(DIR + METAGEN_FILE, 'w') as wf:
        wf.write('\t'.join(['ID_REF'] + run_ids) + '\n')
        for short_group in analysis.keys():
            wf.write('\t'.join([short_group] + analysis[short_group]) + '\n')

def write_maps_file(req_data: list, run_ids: list) -> None:
    """Create the mappings file."""
    with open(DIR + MAPS_FILE, 'w') as wf:
        wf.write(
            '\t'.join([
                'STUDY_ID',
                'SITE_ID',
                'SUBJECT_ID',
                'SAMPLE_CD',
                'PLATFORM',
                'SAMPLE_TYPE',
                'TISSUE_TYPE',
                'TIME_POINT',
                'CATEGORY_CD',
                'SOURCE_CD']) + '\n')
        for i, r in enumerate(req_data):
            subj_id, sample_type = r[0], r[-1]
            path = 'Excluded items+Metagenomic data+' \
                if '-1' in r else 'Metagenomic data+'
            wf.write(
                '\t'.join([
                    'AmericanGutProject',
                    '',
                    str(subj_id),
                    run_ids[i],
                    'platform',
                    sample_type,
                    'tissue_type',
                    'time_point',
                    path + sample_type,
                    'source_cd']) + '\n')

if __name__ == '__main__':
    start_time = time.time()
    
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-s', '--start', type=int, default=0,
        help='start index for sequencing run ids and analysis results '
             'according to file ERP012803_taxonomy_abundances_v2.0.tsv')
    parser.add_argument(
        '-e', '--end', type=int, default=8245,
        help='end index for sequencing run ids and analysis results according '
             'to file ERP012803_taxonomy_abundances_v2.0.tsv')
    args = parser.parse_args()
    if args.end <= args.start:
        sys.exit(
            'The end index for the range of sequencing run ids and analysis '
            'results has to be greater than the start index.')
    
    print('Reading file with analysis results ...', end=' ')
    run_ids, analysis = read_tax_abu_file(args.start, args.end)
    print(u'\u2713')
    
    print('Partitioning sequencing run ids ...', end=' ')
    part_run_ids = part(run_ids)
    print(u'\u2713')
    
    print('Requesting data about subjects and their samples ...', end=' ')
    with multiprocessing.Pool() as p:
        part_req_data = p.map(req_workflow, part_run_ids)
    req_data = functools.reduce(lambda x, y: x+y, part_req_data, [])
    print(u'\u2713')
    
    print('Checking numbers of requested data sets and sequencing run ids ...',
          end=' ')
    check_lens(req_data, run_ids)
    print(u'\u2713')
    
    print('Filtering data sets without host subject id ...', end=' ')
    filt_req_data, filt_run_ids = filt(req_data, run_ids)
    print(u'\u2713')
    
    print('Shortening host subject ids ...', end=' ')
    short_req_data = short_subj_ids(filt_req_data)
    print(u'\u2713')
    
    print('Counting subjects ...', end=' ')
    num_subjs, num_incl, num_excl, num_incl_excl = count_num_subjs(
        short_req_data)
    print(u'\u2713')
    
    print('Counting data sets with unprovided numerical values ...', end=' ')
    num_no_age, num_no_bmi, num_no_height, num_no_weight = count_no_num_data(
        short_req_data)
    print(u'\u2713')
    
    print('Writing annotations file ...', end=' ')
    write_anns_file(analysis)
    print(u'\u2713')
    
    print('Writing subjects files ...', end=' ')
    write_subjs_files(short_req_data)
    print(u'\u2713')
    
    print('Writing metagenomic data file ...', end=' ')
    write_metagen_file(run_ids, analysis)
    print(u'\u2713')
    
    print('Writing mappings file ...', end=' ')
    write_maps_file(short_req_data, run_ids)
    print(u'\u2713')
    
    print(
        ('\n' + 10*'{:36}{:>5}\n').format(
            '# Data sets (unmodified):', len(req_data),
            '# Subjects:', num_subjs,
            '# Data sets without host subject id:',
                len(req_data)-len(filt_req_data),
            '# Data sets without age value:', num_no_age,
            '# Data sets without BMI value:', num_no_bmi,
            '# Data sets without height value:', num_no_height,
            '# Data sets without weight value:', num_no_weight,
            '# Included subjects:', num_incl,
            '# Excluded subjects:', num_excl,
            '# Included/excluded subjects:', num_incl_excl))
    # The last number refers to subjects with at least two data sets
    # each. Based on one data set with and another data set without a
    # missing numerical value, a subject in this group is treated both
    # as included and as excluded.
    
    print(
        'Execution took {} (h:min:s, wall clock time).' \
        .format(datetime.timedelta(seconds=round(time.time() - start_time))))
